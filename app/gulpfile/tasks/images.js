/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *                                                                         *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Ilona Zancaner 1MMPa
 * @created	   8/05/2016
 * @modified
 * @copyright  Copyright © 2014-2015 Artevelde University College Ghent
 *
 * @function   
 *             
 * 			   
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
(gulp => {
    'use strict';

    const CONFIG = require('../config.json');

    let gulp_if = require('gulp-if');

    gulp.task('images', [
        'images:app'
    ]);

    gulp.task('images:app', () => {
        return gulp.src(`${CONFIG.dir.src}images/**/*.+(png|jpg|gif|svg|jpeg)`)
            .pipe(gulp.dest(`${CONFIG.dir.dest}images/`))

    });


})(require('gulp'));