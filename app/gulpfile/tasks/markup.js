/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(gulp => {
    'use strict';

    const CONFIG = require('../config.json');

    gulp.task('markup', [
        'markup:app',
        'markup:templates'
    ]);

    gulp.task('markup:app', () => {
        gulp.src(`${CONFIG.dir.src}*.html`)
            .pipe(gulp.dest(CONFIG.dir.dest));
    });

    gulp.task('markup:templates', () => {
        gulp.src(CONFIG.dir.templates.src)
            .pipe(gulp.dest(CONFIG.dir.templates.dest));
    });

})(require('gulp'));
