/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(gulp => {
    'use strict';

    const CONFIG = require('../config.json');

    let exec = require('child_process').exec;

    gulp.task('default', [
        'default:remove',
        'vendor',
        'markup',
        'scripts',
        'styles',
        'images'
    ]);

    gulp.task('default:remove', () => {
        exec(`rm -rf ${CONFIG.dir.dest}`);
    });

})(require('gulp'));