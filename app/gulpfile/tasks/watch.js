/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(gulp => {
    'use strict';

    const CONFIG = require('../config.json');

    gulp.task('watch', [
        'watch:templates',
        'watch:scripts',
        'watch:styles'
    ]);

    gulp.task('watch:templates', () => {
        gulp.watch(CONFIG.dir.templates.src, ['markup:templates']);
    });

    gulp.task('watch:scripts', () => {
        gulp.watch(`${CONFIG.dir.src}app/**/*.js`, ['scripts:app']);
    });

    gulp.task('watch:styles', () => {
        gulp.watch(`${CONFIG.dir.src}css/**/*.scss`, ['styles:app']);
    });

})(require('gulp'));