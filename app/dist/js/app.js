/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    // Module declarations
    Run.$inject = ['$_', '$faker'];
        NavigationController.$inject = ['$log'];
    SearchController.$inject = ['$log'];
    NavigationDropDownController.$inject = ['$log'];
    SubcategoryDropDownController.$inject = ['$log'];
    angular.module('app', [
        // Angular Module Dependencies
        // ---------------------------
        'ngAnimate',
        'ngMaterial',
        'ngMessages',
        'ngResource',

        // Third-party Module Dependencies
        // -------------------------------
        'ui.router', // Angular UI Router: https://github.com/angular-ui/ui-router/wiki

        // Custom Module Dependencies
        // --------------------------
        'app.about',
        'app.home',
        'app.order',
        'app.products',
        'app.profile',
        'app.services',
        'app.style-guide'
    ]);
    angular.module('app.about', []);
    angular.module('app.home', []);
    angular.module('app.order', []);
    angular.module('app.products', []);
    angular.module('app.profile', []);
    angular.module('app.services', []);
    angular.module('app.style-guide', []);

    // Make wrapper services and remove globals for Third-party Libraries
    angular.module('app')
        .run(Run);

    

    /* @ngInject */
    function Run(
        $_,
        $faker
    ) {}

    

    //Toggle offcanvas navigation
    angular.module('app')
        .controller('NavigationController', NavigationController);

    function NavigationController($log){

        $log.info('NavigationController', Date.now());
        var toggles = document.querySelectorAll('.navigation-toggle');
            console.log(toggles.length);
            if(toggles != null && toggles.length > 0) {

                var toggle = null;
                for(var i = 0; i < toggles.length; i++ ) {
                    toggle = toggles[i];
                    toggle.addEventListener('click', function(ev) {
                        ev.preventDefault();

                        document.querySelector('body').classList.toggle(this.dataset.navtype);
                        document.querySelector('.overlay').classList.toggle('overlay_show');

                        return false;
                    });
                }
            }
        }

    //Toggle searchbar
    angular.module('app')
        .controller('SearchController', SearchController);

    function SearchController($log) {
        $log.info('SearchController' , Date.now());

        var toggles = document.querySelectorAll('.searchbar-toggle');
        console.log(toggles.length);
        if(toggles != null && toggles.length > 0) {
            var toggle = null;
            for(var i = 0; i < toggles.length; i++ ) {
                toggle = toggles[i];
                toggle.addEventListener('click', function(ev) {
                    ev.preventDefault();
                    document.querySelector('body').classList.toggle(this.dataset.searchbar);
                    //document.querySelector('.mobile__header-searchinput').classList.toggle('searchbar_show');
                    //document.querySelector('.mobile__header').classList.toggle('searchbar_show');

                    return false;
                });
            }
        }
    }

    //when clicked on shop in navigation, show categories
    angular.module('app')
        .controller('NavigationDropDownController', NavigationDropDownController);

    function NavigationDropDownController($log) {
        $log.info('NavigationDropDownController' , Date.now());

        var toggles = document.querySelectorAll('.dropdown-toggle');
        console.log(toggles.length);
        if(toggles != null && toggles.length > 0) {
            var toggle = null;
            for(var i = 0; i < toggles.length; i++ ) {
                toggle = toggles[i];
                toggle.addEventListener('click', function(ev) {
                    ev.preventDefault();
                    document.querySelector('body').classList.toggle(this.dataset.navdropdown);
                    //document.querySelector('.mobile__header-searchinput').classList.toggle('searchbar_show');
                    //document.querySelector('.mobile__header').classList.toggle('searchbar_show');

                    return false;
                });
            }
        }
    }
    
    //click on category with subcategories opens hidden dropdown
    angular.module('app')
        .controller('SubcategoryDropDownController', SubcategoryDropDownController);

    function SubcategoryDropDownController($log) {
        $log.info('SubcategoryDropDownController' , Date.now());

        var toggles = document.querySelectorAll('nav li a.subdropdown-toggle');
        console.log(toggles.length);
        if(toggles != null && toggles.length > 0) {
            var toggle = null;
            for(var i = 0; i < toggles.length; i++ ) {
                toggle = toggles[i];
                toggle.addEventListener('click', function(ev) {
                    ev.preventDefault();
                    document.querySelector('body').classList.toggle(this.dataset.subnavdropdown);
                    //this.parentElement.querySelector('.main_nav-subdropdown').classList.toggle('subdropdown-show');
                    //this.classList.toggle('toggle__open');
                    return false;
                });
            }
        }
    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    Config.$inject = ['$compileProvider', '$mdThemingProvider'];
    angular.module('app')
        .config(Config);

    /* @ngInject */
    function Config(
        // Angular
        $compileProvider,
        // Angular Material Design
        $mdThemingProvider
    ) {
        var debug = true; // Set to `false` for production
        $compileProvider.debugInfoEnabled(debug);

        var colour = {
            amber: 'amber',
            blue: 'blue',
            blueGrey: 'blue-grey',
            brown: 'brown',
            cyan: 'cyan',
            deepOrange: 'deep-orange',
            deepPurple: 'deep-purple',
            green: 'green',
            grey: 'grey',
            indigo: 'indigo',
            lightBlue: 'light-blue',
            lightGreen: 'light-green',
            lime: 'lime',
            orange: 'orange',
            pink: 'pink',
            purple: 'purple',
            red: 'red',
            teal: 'teal',
            yellow: 'yellow'
        };

        $mdThemingProvider.theme('default')
            .primaryPalette(colour.blue);

        $mdThemingProvider.theme('alternative')
            .dark()
            .primaryPalette(colour.blue)
            .accentPalette(colour.lime)
            .warnPalette(colour.pink)
        ;
    }
})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    var secure = false;

    angular.module('app')
        .constant('CONFIG', {
            api: {
                protocol: secure ? 'https' : 'http',
                host    : 'www.munch.local',
                path    : '/api/v1/'
            }
        });
})();
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *                                                                         *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Ilona Zancaner 1MMPa
 * @created	   3/05/2016
 * @modified
 * @copyright  Copyright © 2014-2015 Artevelde University College Ghent
 *
 * @function   
 *             
 * 			   
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 *	Title: Munch App
 *	Modified: 23-03-2016
 *	Version: 1.0.0
 *	Author: Ilona Zancaner
 * 	-----------------------------------------------
 */

(function() {



    // Describe an App object with own functionalities
    var App = {
        init: function() {
            var self = this;

            //this.registerNavigationToggleListeners();// Register All Navigation Toggle Listeners

            //this.registerWindowListeners();// Register All Navigation Toggle Listeners

        },
        registerNavigationToggleListeners: function() {

            var toggles = document.querySelectorAll('.navigation-toggle');
console.log(toggles.length);
            if(toggles != null && toggles.length > 0) {

                var toggle = null;
                alert("klik");
                for(var i = 0; i < toggles.length; i++ ) {
                    toggle = toggles[i];
                    toggle.addEventListener('click', function(ev) {
                        ev.preventDefault();

                        document.querySelector('body').classList.toggle(this.dataset.navtype);
                        document.querySelector('.overlay').classList.toggle('.overlay_show');

                        return false;
                    });
                }
            }
        },
        registerWindowListeners: function() {
            window.addEventListener('resize', function(ev) {
                if(document.querySelector('body').classList.contains('offcanvas-open')) {
                    document.querySelector('body').classList.remove('offcanvas-open');
                }
            });
        }

    };

    App.init();// Intialize the application


})();
/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    Routes.$inject = ['$urlRouterProvider'];
    angular.module('app')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $urlRouterProvider
    ) {
        $urlRouterProvider.otherwise('/');
    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    AboutController.$inject = ['$log'];
    angular.module('app.about')
        .controller('AboutController', AboutController);

    /* @ngInject */
    function AboutController(
        // Angular
        $log
    ) {
        $log.info('AboutController', Date.now());

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  },
                { label: 'shop'       , uri: '#'            , state: 'shop'  },
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  },
                { label: 'groenten'       , uri: '#/products'       , state: 'products'  },
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  },
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  },
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  },
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' }
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ],
            title: 'Home',
            footermenu: [
                {label:'about',     uri: '#/about',         state:'about'},
                {label:'contact',   uri: '#/contact', state:'contact'}
            ]
        }
    }

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    Routes.$inject = ['$stateProvider'];
    angular.module('app.about')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('about', {
                cache: false, // false will reload on every visit.
                controller: 'AboutController as vm',
                templateUrl: 'html/about/about.view.html',
                url: '/about'
            })
            .state('contact', {
                cache: false, // false will reload on every visit.
                controller: 'AboutController as vm',
                templateUrl: 'html/about/contact.view.html',
                url: '/about/contact'
            })
            .state('disclaimer', {
                cache: false, // false will reload on every visit.
                controller: 'AboutController as vm',
                templateUrl: 'html/about/disclaimer.view.html',
                url: '/about/disclaimer'
            });

    }

})();
/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    HomeController.$inject = ['$log'];
    angular.module('app.home')
        .controller('HomeController', HomeController);

    /* @ngInject */
    function HomeController(
        // Angular
        $log
    ) {
        $log.info('HomeController', Date.now());
    
        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  ,class: ''},
                { label: 'shop'       , uri: '#'            , state: 'shop'  ,class: 'listitem-droplist'},
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  ,class: ''},
                { label: 'groenten'       , uri: '#/products'       , state: 'products'  ,class: ''},
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  ,class: ''},
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  ,class: ''},
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  ,class: ''},
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' ,class: ''}
            ],
            submenu: [
                { label: 'groenten'       , uri: '#/products'            , state: 'vegetables'  ,class: 'listitem-go'},
                { label: 'fruit'       , uri: '#/products'            , state: 'vegetables'  ,class: 'listitem-go'},
                { label: 'vlees'       , uri: '#/products'            , state: 'vegetables'  ,class: 'listitem-droplist'}
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ],
            title: 'Home',
            footermenu: [
                {label:'about',     uri: '#/about',         state:'about'},
                {label:'contact',   uri: '#/contact', state:'contact'}
            ],
            homeCategoriesLeft: [
                {label: 'Groenten', source: 'images/groenten.jpg', state:'products.vegetables'},
                {label: 'Vlees', source: 'images/vlees.jpg', state:'products.meat'},
                {label: 'Fruit', source: 'images/fruit.jpeg', state:'products.fruit'},
                {label: 'Zuivel', source: 'images/zuivel.jpg', state:'products.dairy'},
                {label: 'Granen', source: 'images/granen.jpg', state:'products.cereal'}
            ],
            homeCategoriesRight: [

            ]
        }
    }

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    Routes.$inject = ['$stateProvider'];
    angular.module('app.home')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('home', {
                cache: false, // false will reload on every visit.
                controller: 'HomeController as vm',
                templateUrl: 'html/home/home.view.html',
                url: '/'
            });
    }

})();
/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    CategoryResource.$inject = ['$resource', 'UriService'];
    angular.module('app.products')
        .factory('CategoryResource', CategoryResource);

    /* @ngInject */
    function CategoryResource(
        // Angular
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('categories/:category_id');
        var paramDefaults = {
            category_id : '@id'
        };

        var actions = {};
        
        return $resource(url, paramDefaults, actions);
    }

})();
/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */

;(function () {
    'use strict';

    DetailController.$inject = ['$log', '$state', 'CategoryResource', 'ProductResource'];
    angular.module('app.products')
        .controller('DetailController', DetailController);

    /* @ngInject */
    function DetailController(
        // Angular
        $log,
        // Third-party Services
        $state,
        // Custom
        CategoryResource,
        ProductResource
    ) {
        $log.info('DetailController', Date.now());
        $log.log('Current state name:', $state.current.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  },
                { label: 'shop'       , uri: '#'            , state: 'shop'  },
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  },
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  },
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  },
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  },
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' }
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ],
            footermenu: [
                {label:'about',     uri: '#/about',         state:'about'},
                {label:'contact',   uri: '#/contact', state:'contact'}
            ]
        };

        // Data
        // ----
        vm.product = {
        };

        vm.av_rating = [];

        // States
        // ------
        switch ($state.current.name) {
            case 'detail':
                vm.product = getProductAction();
                break;
            default:
                break;
        }

        // Functions
        // =========

        // Products
        // -----

        function getProductAction() {
            $log.log(getProductAction.name);

            var params = {
                product_id: $state.params.product_id
            };

            console.log(params.product_id);
            return ProductResource
                .get(params, success, error);

            function error(error) {
                $log.error(getProductAction.name, 'ERROR', 'error:', error);
            }

            function success(resource, responseHeader) {
                $log.log(getProductAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());

            }

        }

        function getRating(rating){

            console.log(rating);
            var ratingArray = [];
            for (var i = 1; i <= rating; i++) {
                ratingArray.push(i);
            }
            console.log(ratingArray);
            return ratingArray;
        }




    }


})();
/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    ProductResource.$inject = ['$resource', 'UriService'];
    angular.module('app.products')
        .factory('ProductResource', ProductResource);

    /* @ngInject */
    function ProductResource(
        // Angular
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('products/:product_id');

        var paramDefaults = {
            product_id: '@id' // @id is the property, e.g. `product.id` if the resource instance is `product`.
        };

        

        var actions = {
            'update': {
                method: 'PUT'
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();


/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */


;(function () {
    'use strict';

    ProductsController.$inject = ['$log', '$state', '$faker', 'CategoryResource', 'ProductResource'];
    angular.module('app.products')
        .controller('ProductsController', ProductsController);
    

    /* @ngInject */
    function ProductsController(
        // Angular
        $log,
        // Third-party Services
        $state,
        // Third-party Libraries
        $faker,
        // Custom
        CategoryResource,
        ProductResource
    ) {
        $log.info('ProductsController', Date.now());
        $log.log('Current state name:', $state.current.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {
            remove: RemoveCartItem
        };

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  },
                { label: 'shop'       , uri: '#'            , state: 'shop'  },
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  },
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  },
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  },
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  },
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' }
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ],
            footermenu: [
                {label:'about',     uri: '#/about',         state:'about'},
                {label:'contact',   uri: '#/contact', state:'contact'}
            ]
        };

        // Data
        // ----
        vm.products = {};

        // States
        // ------
        switch ($state.current.name) {
            case 'products':
                vm.products = getProductsAction();
                break;
            default:
                break;
        }

        // Functions
        // =========

        // Products
        // -----

        function getProductsAction() {
            $log.info(getProductsAction.name);

            //vm.$$ui.loader = true;

            return ProductResource
                .query(success, error);

            function error(error) {
                $log.error(getProductsAction.name, 'ERROR', 'error:', error);
                //vm.$$ui.loader = false;

            }

            function success(resource, responseHeader) {
                $log.log(getProductsAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                //vm.$$ui.loader = false;
            }
        }

        function RemoveCartItem(cartitem) {
            _.remove($vm.cartitems, function(c){
                return c.id == cartitem.id;
            });
        }


    }


})();


/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    Routes.$inject = ['$stateProvider'];
    angular.module('app.products')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('products', {
                cache: false, // false will reload on every visit.
                controller: 'ProductsController as vm',
                templateUrl: 'html/products/products.view.html',
                url: '/products'
            })
            .state('basket', {
                cache: false, // false will reload on every visit.
                controller: 'ProductsController as vm',
                templateUrl: 'html/products/basket.view.html',
                url: '/products/basket'
            })
            .state('detail', {
                cache: false, // false will reload on every visit.
                controller: 'DetailController as vm',
                templateUrl: 'html/products/detail.view.html',
                url: '/products/{product_id:int}'
            });
    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    OrderController.$inject = ['$log'];
    angular.module('app.order')
        .controller('OrderController', OrderController);

    /* @ngInject */
    function OrderController(
        // Angular
        $log
    ) {
        $log.info('OrderController', Date.now());

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  },
                { label: 'shop'       , uri: '#'            , state: 'shop'  },
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  },
                { label: 'groenten'       , uri: '#/products'       , state: 'products'  },
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  },
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  },
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  },
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' }
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ]
        }
    }

})();

/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */

;(function () {
    'use strict';

    Routes.$inject = ['$stateProvider'];
    angular.module('app.order')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('information', {
                cache: false, // false will reload on every visit.
                controller: 'OrderController as vm',
                templateUrl: 'html/order/information.view.html',
                url: '/order/information'
            })
            .state('ordercustomer', {
                cache: false, // false will reload on every visit.
                controller: 'OrderController as vm',
                templateUrl: 'html/order/ordercustomer.view.html',
                url: '/order/login_register'
            })
            .state('ordernewcustomer', {
                cache: false, // false will reload on every visit.
                controller: 'OrderController as vm',
                templateUrl: 'html/order/ordernewcustomer.view.html',
                url: '/order/new_customer'
            })
            .state('ordercomplete', {
                cache: false, // false will reload on every visit.
                controller: 'OrderController as vm',
                templateUrl: 'html/order/ordercomplete.view.html',
                url: '/order/ordercomplete'
            });
    }

})();
/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    CustomerResource.$inject = ['$cacheFactory', '$resource', 'UriService'];
    angular.module('app.profile')
        .factory('CustomerResource', CustomerResource);

    /* @ngInject */
    function CustomerResource(
        // Angular
        $cacheFactory,
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('customers/:customer_id'),
            cache = $cacheFactory('CategoryResource');

        var paramDefaults = {
            customer_id: '@id' // @id is the property, e.g. `product.id` if the resource instance is `product`.
        };

        var actions = {
            'query': {
                cache: cache,
                method: 'GET',
                isArray: true
            },
            'update': {
                method: 'PUT'
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();
/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    OrderResource.$inject = ['$cacheFactory', '$resource', 'UriService'];
    angular.module('app.profile')
        .factory('OrderResource', OrderResource);

    /* @ngInject */
    function OrderResource(
        // Angular
        $cacheFactory,
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('customers/:customer_id/orders/:order_id/'),
            cache = $cacheFactory('OrderResource');

        var paramDefaults = {
            // @id is the property, e.g. `product.id` if the resource instance is `product`.
            customer_id: '@customer_id',
            order_id: '@id'
        };

        var actions = {
            'query': {
                cache: cache,
                method: 'GET',
                isArray: true
            },
            'update': {
                method: 'PUT'
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();


/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */


;(function () {
    'use strict';

    OrderController.$inject = ['$log', '$state', '$faker', 'OrderResource', 'UriService'];
    angular.module('app.profile')
        .controller('OrderController', OrderController);

    /* @ngInject */
    function OrderController(
        // Angular
        $log,
        // Third-party Services
        $state,
        // Third-party Libraries
        $faker,
        // Custom
        OrderResource,
        UriService
    ) {
        $log.info('OrderController', Date.now());
        $log.log('Current state name:', $state.current.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {

        };

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  },
                { label: 'shop'       , uri: '#'            , state: 'shop'  },
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  },
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  },
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  },
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  },
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' }
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ],
            footermenu: [
                {label:'about',     uri: '#/about',         state:'about'},
                {label:'contact',   uri: '#/contact', state:'contact'}
            ]
        };

        // Data
        // ----


        vm.order = {};

        // States
        // ------
        switch ($state.current.name) {
            case 'order':
                vm.order = getOrderAction();
                break;
            default:
                break;
        }

        // Functions
        // =========

        // Products
        // -----
        

        vm.calculateSaldo = function(price, quantity) {
            var saldo = parseFloat(price)*parseFloat(quantity);
            saldo = Math.round(saldo * 100) / 100;
            return saldo;
        };
        


        // Order
        // -----

        function getOrderAction() {
            $log.log(getOrderAction.name);

            var params = {
                order_id: $state.params.order_id

            };

            console.log(params);

            return OrderResource
                .query(params, success, error);

            function error(error) {
                $log.error(getOrderAction.name, 'ERROR', 'error:', error);
            }

            function success(resource, responseHeader) {
                $log.log(getOrderAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());

            }

        }

    }


})();




/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */


;(function () {
    'use strict';

    OrdersController.$inject = ['$log', '$state', '$faker', 'OrderResource', 'UriService'];
    angular.module('app.profile')
        .controller('OrdersController', OrdersController);


    /* @ngInject */
    function OrdersController(
        // Angular
        $log,
        // Third-party Services
        $state,
        // Third-party Libraries
        $faker,
        // Custom
        OrderResource,
        UriService
    ) {
        $log.info('OrdersController', Date.now());
        $log.log('Current state name:', $state.current.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {

        };

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  },
                { label: 'shop'       , uri: '#'            , state: 'shop'  },
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  },
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  },
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  },
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  },
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' }
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ],
            footermenu: [
                {label:'about',     uri: '#/about',         state:'about'},
                {label:'contact',   uri: '#/contact', state:'contact'}
            ]
        };

        // Data
        // ----
        vm.orders = {};

        vm.order = {};

        // States
        // ------
        switch ($state.current.name) {
            case 'orders':
                vm.orders = getOrdersAction();
                break;
            case 'order':
                vm.order = getOrderDetailAction();
                break;
            default:
                break;
        }

        // Functions
        // =========

        // Products
        // -----

        function getOrdersAction() {
            $log.info(getOrdersAction.name);

            //vm.$$ui.loader = true;

            var params = {
                customer_id: $state.params.customer_id
            };

            console.log(params.customer_id);

            return OrderResource.query(params, success, error);

            function error(error) {
                $log.error(getOrdersAction.name, 'ERROR', 'error:', error);
                //vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getOrdersAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                //vm.$$ui.loader = false;
            }
        }

        vm.calculateSaldo = function(price, quantity) {
            var saldo = parseFloat(price)*parseFloat(quantity);
            saldo = Math.round(saldo * 100) / 100;
            return saldo;
        };


        // Order
        // -----

        function getOrderDetailAction() {
            $log.log(getOrderDetailAction.name);

            var params = {
                customer_id: $state.params.customer_id,
                order_id: $state.params.order_id

            };

            console.log(params);

            return OrderResource
                .query(params, success, error);

            function error(error) {
                $log.error(getOrderDetailAction.name, 'ERROR', 'error:', error);
            }

            function success(resource, responseHeader) {
                $log.log(getOrderDetailAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());

            }

        }

    }


})();


/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */

;(function () {
    'use strict';

    ProfileController.$inject = ['$cacheFactory', '$log', '$state', 'CustomerResource', 'UriService'];
    angular.module('app.profile')
        .controller('ProfileController', ProfileController);

    /* @ngInject */
    function ProfileController(
        // Angular
        $cacheFactory,
        $log,
        // Third-party Services
        $state,
        // Custom
        CustomerResource,
        UriService
    ) {

        $log.info('ProfileController', Date.now());
        $log.log('Current state name:', $state.current.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  },
                { label: 'shop'       , uri: '#'            , state: 'shop'  },
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  },
                { label: 'groenten'       , uri: '#/products'       , state: 'products'  },
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  },
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  },
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  },
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' }
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ],
            title: 'Home',
            footermenu: [
                {label:'about',     uri: '#/about',         state:'about'},
                {label:'contact',   uri: '#/contact', state:'contact'}
            ]
        };

        // Data
        // ----
        vm.customer = new CustomerResource();

        // States
        // ------
        switch ($state.current.name) {
            case 'profile':
                vm.customer = getCustomerAction();
                break;
            case 'update':
                vm.$$ix.save = updateCustomerAction;
                vm.customer = getCustomerAction();
                break;
            case 'register':
                vm.$$ix.save = createCustomerAction;
                break;
            default:
                break;
        }

        // Functions
        // =========

        // Customer
        // -----

        function getCustomerAction() {
            $log.log(getCustomerAction.name);

            var params = {
                customer_id: $state.params.customer_id
            };

            console.log(params);
            return CustomerResource
                .get(params, success, error);

            function error(error) {
                $log.error(getCustomerAction.name, 'ERROR', 'error:', error);
            }

            function success(resource, responseHeader) {
                $log.log(getCustomerAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());

            }

        }

        function createCustomerAction() {
            $log.info(createCustomerAction.name, 'customer:', vm.customer);

            vm.customer.$save(success, error);

            function error(error) {
                $log.error(createCustomerAction.name, 'ERROR', 'error:', error);
            }

            function success(resource, responseHeader) {
                $log.log(createCustomerAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                updateResourceCache(resource, 'CustomerResource', 'customers');
                $state.go('profile', {}, { reload: true });
            }

            function updateResourceCache(resource, cacheId, cacheKeyId) {
                var cacheKey = UriService.getApi(cacheKeyId); // Cache key is based on API URI.
                var cache = $cacheFactory.get(cacheId);
                var cachedResources = cache.get(cacheKey);
                var resources = JSON.parse(cachedResources[1]);
                resources.push(resource.toJSON());
                cachedResources[1] = JSON.stringify(resources);
                cache.put(cacheKey, cachedResources);
            }
        }

        function updateCustomerAction() {
            $log.info(updateCustomerAction.name, 'customer:', vm.customer);

            vm.customer.$update(success, error);

            function error(error) {
                $log.error(updateCustomerAction.name, 'ERROR:', error);
            }

            function success(resource, responseHeader) {
                $log.log(updateCustomerAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
            }
        }

    }

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.profile')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('profile', {
                cache: false, // false will reload on every visit.
                controller: 'ProfileController as vm',
                templateUrl: 'html/profile/profile.view.html',
                url: '/profile/{customer_id:int}'
            })
            .state('update', {
                    cache: false, // false will reload on every visit.
                    controller: 'ProfileController as vm',
                    templateUrl: 'html/profile/account.view.html',
                    url: '/profile/{customer_id:int}/update'
             })
            .state('orders', {
                cache: false, // false will reload on every visit.
                controller: 'OrdersController as vm',
                templateUrl: 'html/profile/orders.view.html',
                url: '/profile/{customer_id:int}/orders'
            })
            .state('order', {
                cache: false, // false will reload on every visit.
                controller: 'OrdersController as vm',
                templateUrl: 'html/profile/order.view.html',
                url: '/profile/{order_id:int}/orders/{customer_id:int}'
            })
            .state('favorites', {
                cache: false, // false will reload on every visit.
                controller: 'ProfileController as vm',
                templateUrl: 'html/profile/favorites.view.html',
                url: '/profile/{customer_id:int}/favorites'
            })
            .state('login', {
                cache: false, // false will reload on every visit.
                controller: 'ProfileController as vm',
                templateUrl: 'html/profile/login.view.html',
                url: '/login'
            })
            .state('register', {
                cache: false, // false will reload on every visit.
                controller: 'ProfileController as vm',
                templateUrl: 'html/profile/register.view.html',
                url: '/register'
            })
    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    faker.$inject = ['$window'];
    angular.module('app.services')
        .factory('$faker', faker);

    /* @ngInject */
    function faker(
        // Angular
        $window
    ) {
        var faker = $window.faker;

        delete window.faker;
        delete $window.faker;

        return faker;
    }

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    lodash.$inject = ['$window'];
    angular.module('app.services')
        .factory('$_', lodash);

    /* @ngInject */
    function lodash(
        // Angular
        $window
    ) {
        var _ = $window._;

        delete window._;
        delete $window._;

        return _;
    }

})();

/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    UriService.$inject = ['$location', 'CONFIG'];
    angular.module('app.services')
        .factory('UriService', UriService);

    /* @ngInject */
    function UriService(
        // Angular
        $location,
        // Custom
        CONFIG
    ) {
        return {
            getApi: getApi
        };

        function getApi(path) {
            var protocol = CONFIG.api.protocol ? CONFIG.api.protocol : $location.protocol(),
                host     = CONFIG.api.host     ? CONFIG.api.host     : $location.host(),
                uri      = protocol + '://' + host + CONFIG.api.path + path;

            return uri;
        }

    }

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    StyleGuideController.$inject = ['$log', '$scope', '$_', '$faker'];
    angular.module('app.style-guide')
        .controller('StyleGuideController', StyleGuideController);

    /* @ngInject */
    function StyleGuideController(
        // Angular
        $log,
        $scope,
        // Third-party
        $_,
        $faker
    ) {
        $log.info(StyleGuideController.name, Date.now());

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {
        };

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Style Guide'
        };

        // Data
        // ----
        vm.product = {
            title: null,
            content: null,
            category: null,
            tags: []
        };

        // Functions
        // =========
        
        // Actions
        // -------
        function saveAction() {
            $log.log('saveAction:', vm.product);
        }

        function cancelAction() {
            $log.log('cancelAction');
        }

        // Watchers
        // --------

        // Other
        // -----

    }

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.style-guide', []);

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    Routes.$inject = ['$stateProvider'];
    angular.module('app.style-guide')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('style-guide', {
                cache: false, // false will reload on every visit.
                controller: 'StyleGuideController as vm',
                templateUrl: 'html/style-guide/style-guide.view.html',
                url: '/style-guide'
            });
    }

})();