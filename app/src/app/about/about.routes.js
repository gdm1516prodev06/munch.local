/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.about')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('about', {
                cache: false, // false will reload on every visit.
                controller: 'AboutController as vm',
                templateUrl: 'html/about/about.view.html',
                url: '/about'
            })
            .state('contact', {
                cache: false, // false will reload on every visit.
                controller: 'AboutController as vm',
                templateUrl: 'html/about/contact.view.html',
                url: '/about/contact'
            })
            .state('disclaimer', {
                cache: false, // false will reload on every visit.
                controller: 'AboutController as vm',
                templateUrl: 'html/about/disclaimer.view.html',
                url: '/about/disclaimer'
            });

    }

})();