/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    // Module declarations
    angular.module('app', [
        // Angular Module Dependencies
        // ---------------------------
        'ngAnimate',
        'ngMaterial',
        'ngMessages',
        'ngResource',

        // Third-party Module Dependencies
        // -------------------------------
        'ui.router', // Angular UI Router: https://github.com/angular-ui/ui-router/wiki

        // Custom Module Dependencies
        // --------------------------
        'app.about',
        'app.home',
        'app.order',
        'app.products',
        'app.profile',
        'app.services',
        'app.style-guide'
    ]);
    angular.module('app.about', []);
    angular.module('app.home', []);
    angular.module('app.order', []);
    angular.module('app.products', []);
    angular.module('app.profile', []);
    angular.module('app.services', []);
    angular.module('app.style-guide', []);

    // Make wrapper services and remove globals for Third-party Libraries
    angular.module('app')
        .run(Run);

    

    /* @ngInject */
    function Run(
        $_,
        $faker
    ) {}

    

    //Toggle offcanvas navigation
    angular.module('app')
        .controller('NavigationController', NavigationController);

    function NavigationController($log){

        $log.info('NavigationController', Date.now());
        var toggles = document.querySelectorAll('.navigation-toggle');
            console.log(toggles.length);
            if(toggles != null && toggles.length > 0) {

                var toggle = null;
                for(var i = 0; i < toggles.length; i++ ) {
                    toggle = toggles[i];
                    toggle.addEventListener('click', function(ev) {
                        ev.preventDefault();

                        document.querySelector('body').classList.toggle(this.dataset.navtype);
                        document.querySelector('.overlay').classList.toggle('overlay_show');

                        return false;
                    });
                }
            }
        }

    //Toggle searchbar
    angular.module('app')
        .controller('SearchController', SearchController);

    function SearchController($log) {
        $log.info('SearchController' , Date.now());

        var toggles = document.querySelectorAll('.searchbar-toggle');
        console.log(toggles.length);
        if(toggles != null && toggles.length > 0) {
            var toggle = null;
            for(var i = 0; i < toggles.length; i++ ) {
                toggle = toggles[i];
                toggle.addEventListener('click', function(ev) {
                    ev.preventDefault();
                    document.querySelector('body').classList.toggle(this.dataset.searchbar);
                    //document.querySelector('.mobile__header-searchinput').classList.toggle('searchbar_show');
                    //document.querySelector('.mobile__header').classList.toggle('searchbar_show');

                    return false;
                });
            }
        }
    }

    //when clicked on shop in navigation, show categories
    angular.module('app')
        .controller('NavigationDropDownController', NavigationDropDownController);

    function NavigationDropDownController($log) {
        $log.info('NavigationDropDownController' , Date.now());

        var toggles = document.querySelectorAll('.dropdown-toggle');
        console.log(toggles.length);
        if(toggles != null && toggles.length > 0) {
            var toggle = null;
            for(var i = 0; i < toggles.length; i++ ) {
                toggle = toggles[i];
                toggle.addEventListener('click', function(ev) {
                    ev.preventDefault();
                    document.querySelector('body').classList.toggle(this.dataset.navdropdown);
                    //document.querySelector('.mobile__header-searchinput').classList.toggle('searchbar_show');
                    //document.querySelector('.mobile__header').classList.toggle('searchbar_show');

                    return false;
                });
            }
        }
    }
    
    //click on category with subcategories opens hidden dropdown
    angular.module('app')
        .controller('SubcategoryDropDownController', SubcategoryDropDownController);

    function SubcategoryDropDownController($log) {
        $log.info('SubcategoryDropDownController' , Date.now());

        var toggles = document.querySelectorAll('nav li a.subdropdown-toggle');
        console.log(toggles.length);
        if(toggles != null && toggles.length > 0) {
            var toggle = null;
            for(var i = 0; i < toggles.length; i++ ) {
                toggle = toggles[i];
                toggle.addEventListener('click', function(ev) {
                    ev.preventDefault();
                    document.querySelector('body').classList.toggle(this.dataset.subnavdropdown);
                    //this.parentElement.querySelector('.main_nav-subdropdown').classList.toggle('subdropdown-show');
                    //this.classList.toggle('toggle__open');
                    return false;
                });
            }
        }
    }

})();