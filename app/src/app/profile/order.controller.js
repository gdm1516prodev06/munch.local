

/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */


;(function () {
    'use strict';

    angular.module('app.profile')
        .controller('OrderController', OrderController);

    /* @ngInject */
    function OrderController(
        // Angular
        $log,
        // Third-party Services
        $state,
        // Third-party Libraries
        $faker,
        // Custom
        OrderResource,
        UriService
    ) {
        $log.info('OrderController', Date.now());
        $log.log('Current state name:', $state.current.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {

        };

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  },
                { label: 'shop'       , uri: '#'            , state: 'shop'  },
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  },
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  },
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  },
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  },
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' }
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ],
            footermenu: [
                {label:'about',     uri: '#/about',         state:'about'},
                {label:'contact',   uri: '#/contact', state:'contact'}
            ]
        };

        // Data
        // ----


        vm.order = {};

        // States
        // ------
        switch ($state.current.name) {
            case 'order':
                vm.order = getOrderAction();
                break;
            default:
                break;
        }

        // Functions
        // =========

        // Products
        // -----
        

        vm.calculateSaldo = function(price, quantity) {
            var saldo = parseFloat(price)*parseFloat(quantity);
            saldo = Math.round(saldo * 100) / 100;
            return saldo;
        };
        


        // Order
        // -----

        function getOrderAction() {
            $log.log(getOrderAction.name);

            var params = {
                order_id: $state.params.order_id

            };

            console.log(params);

            return OrderResource
                .query(params, success, error);

            function error(error) {
                $log.error(getOrderAction.name, 'ERROR', 'error:', error);
            }

            function success(resource, responseHeader) {
                $log.log(getOrderAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());

            }

        }

    }


})();

