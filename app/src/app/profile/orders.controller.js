

/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */


;(function () {
    'use strict';

    angular.module('app.profile')
        .controller('OrdersController', OrdersController);


    /* @ngInject */
    function OrdersController(
        // Angular
        $log,
        // Third-party Services
        $state,
        // Third-party Libraries
        $faker,
        // Custom
        OrderResource,
        UriService
    ) {
        $log.info('OrdersController', Date.now());
        $log.log('Current state name:', $state.current.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {

        };

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  },
                { label: 'shop'       , uri: '#'            , state: 'shop'  },
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  },
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  },
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  },
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  },
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' }
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ],
            footermenu: [
                {label:'about',     uri: '#/about',         state:'about'},
                {label:'contact',   uri: '#/contact', state:'contact'}
            ]
        };

        // Data
        // ----
        vm.orders = {};

        vm.order = {};

        // States
        // ------
        switch ($state.current.name) {
            case 'orders':
                vm.orders = getOrdersAction();
                break;
            case 'order':
                vm.order = getOrderDetailAction();
                break;
            default:
                break;
        }

        // Functions
        // =========

        // Products
        // -----

        function getOrdersAction() {
            $log.info(getOrdersAction.name);

            //vm.$$ui.loader = true;

            var params = {
                customer_id: $state.params.customer_id
            };

            console.log(params.customer_id);

            return OrderResource.query(params, success, error);

            function error(error) {
                $log.error(getOrdersAction.name, 'ERROR', 'error:', error);
                //vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getOrdersAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                //vm.$$ui.loader = false;
            }
        }

        vm.calculateSaldo = function(price, quantity) {
            var saldo = parseFloat(price)*parseFloat(quantity);
            saldo = Math.round(saldo * 100) / 100;
            return saldo;
        };


        // Order
        // -----

        function getOrderDetailAction() {
            $log.log(getOrderDetailAction.name);

            var params = {
                customer_id: $state.params.customer_id,
                order_id: $state.params.order_id

            };

            console.log(params);

            return OrderResource
                .query(params, success, error);

            function error(error) {
                $log.error(getOrderDetailAction.name, 'ERROR', 'error:', error);
            }

            function success(resource, responseHeader) {
                $log.log(getOrderDetailAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());

            }

        }

    }


})();

