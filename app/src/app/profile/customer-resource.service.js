/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.profile')
        .factory('CustomerResource', CustomerResource);

    /* @ngInject */
    function CustomerResource(
        // Angular
        $cacheFactory,
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('customers/:customer_id'),
            cache = $cacheFactory('CategoryResource');

        var paramDefaults = {
            customer_id: '@id' // @id is the property, e.g. `product.id` if the resource instance is `product`.
        };

        var actions = {
            'query': {
                cache: cache,
                method: 'GET',
                isArray: true
            },
            'update': {
                method: 'PUT'
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();