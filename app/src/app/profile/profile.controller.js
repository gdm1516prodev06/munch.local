/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */

;(function () {
    'use strict';

    angular.module('app.profile')
        .controller('ProfileController', ProfileController);

    /* @ngInject */
    function ProfileController(
        // Angular
        $cacheFactory,
        $log,
        // Third-party Services
        $state,
        // Custom
        CustomerResource,
        UriService
    ) {

        $log.info('ProfileController', Date.now());
        $log.log('Current state name:', $state.current.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  },
                { label: 'shop'       , uri: '#'            , state: 'shop'  },
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  },
                { label: 'groenten'       , uri: '#/products'       , state: 'products'  },
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  },
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  },
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  },
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' }
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ],
            title: 'Home',
            footermenu: [
                {label:'about',     uri: '#/about',         state:'about'},
                {label:'contact',   uri: '#/contact', state:'contact'}
            ]
        };

        // Data
        // ----
        vm.customer = new CustomerResource();

        // States
        // ------
        switch ($state.current.name) {
            case 'profile':
                vm.customer = getCustomerAction();
                break;
            case 'update':
                vm.$$ix.save = updateCustomerAction;
                vm.customer = getCustomerAction();
                break;
            case 'register':
                vm.$$ix.save = createCustomerAction;
                break;
            default:
                break;
        }

        // Functions
        // =========

        // Customer
        // -----

        function getCustomerAction() {
            $log.log(getCustomerAction.name);

            var params = {
                customer_id: $state.params.customer_id
            };

            console.log(params);
            return CustomerResource
                .get(params, success, error);

            function error(error) {
                $log.error(getCustomerAction.name, 'ERROR', 'error:', error);
            }

            function success(resource, responseHeader) {
                $log.log(getCustomerAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());

            }

        }

        function createCustomerAction() {
            $log.info(createCustomerAction.name, 'customer:', vm.customer);

            vm.customer.$save(success, error);

            function error(error) {
                $log.error(createCustomerAction.name, 'ERROR', 'error:', error);
            }

            function success(resource, responseHeader) {
                $log.log(createCustomerAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                updateResourceCache(resource, 'CustomerResource', 'customers');
                $state.go('profile', {}, { reload: true });
            }

            function updateResourceCache(resource, cacheId, cacheKeyId) {
                var cacheKey = UriService.getApi(cacheKeyId); // Cache key is based on API URI.
                var cache = $cacheFactory.get(cacheId);
                var cachedResources = cache.get(cacheKey);
                var resources = JSON.parse(cachedResources[1]);
                resources.push(resource.toJSON());
                cachedResources[1] = JSON.stringify(resources);
                cache.put(cacheKey, cachedResources);
            }
        }

        function updateCustomerAction() {
            $log.info(updateCustomerAction.name, 'customer:', vm.customer);

            vm.customer.$update(success, error);

            function error(error) {
                $log.error(updateCustomerAction.name, 'ERROR:', error);
            }

            function success(resource, responseHeader) {
                $log.log(updateCustomerAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
            }
        }

    }

})();
