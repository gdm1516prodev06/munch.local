/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.profile')
        .factory('OrderResource', OrderResource);

    /* @ngInject */
    function OrderResource(
        // Angular
        $cacheFactory,
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('customers/:customer_id/orders/:order_id/'),
            cache = $cacheFactory('OrderResource');

        var paramDefaults = {
            // @id is the property, e.g. `product.id` if the resource instance is `product`.
            customer_id: '@customer_id',
            order_id: '@id'
        };

        var actions = {
            'query': {
                cache: cache,
                method: 'GET',
                isArray: true
            },
            'update': {
                method: 'PUT'
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();