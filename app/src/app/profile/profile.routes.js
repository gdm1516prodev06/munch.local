/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.profile')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('profile', {
                cache: false, // false will reload on every visit.
                controller: 'ProfileController as vm',
                templateUrl: 'html/profile/profile.view.html',
                url: '/profile/{customer_id:int}'
            })
            .state('update', {
                    cache: false, // false will reload on every visit.
                    controller: 'ProfileController as vm',
                    templateUrl: 'html/profile/account.view.html',
                    url: '/profile/{customer_id:int}/update'
             })
            .state('orders', {
                cache: false, // false will reload on every visit.
                controller: 'OrdersController as vm',
                templateUrl: 'html/profile/orders.view.html',
                url: '/profile/{customer_id:int}/orders'
            })
            .state('order', {
                cache: false, // false will reload on every visit.
                controller: 'OrdersController as vm',
                templateUrl: 'html/profile/order.view.html',
                url: '/profile/{order_id:int}/orders/{customer_id:int}'
            })
            .state('favorites', {
                cache: false, // false will reload on every visit.
                controller: 'ProfileController as vm',
                templateUrl: 'html/profile/favorites.view.html',
                url: '/profile/{customer_id:int}/favorites'
            })
            .state('login', {
                cache: false, // false will reload on every visit.
                controller: 'ProfileController as vm',
                templateUrl: 'html/profile/login.view.html',
                url: '/login'
            })
            .state('register', {
                cache: false, // false will reload on every visit.
                controller: 'ProfileController as vm',
                templateUrl: 'html/profile/register.view.html',
                url: '/register'
            })
    }

})();