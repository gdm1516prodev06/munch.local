

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */


;(function () {
    'use strict';

    angular.module('app.products')
        .controller('ProductsController', ProductsController);
    

    /* @ngInject */
    function ProductsController(
        // Angular
        $log,
        // Third-party Services
        $state,
        // Third-party Libraries
        $faker,
        // Custom
        CategoryResource,
        ProductResource
    ) {
        $log.info('ProductsController', Date.now());
        $log.log('Current state name:', $state.current.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {
            remove: RemoveCartItem
        };

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  },
                { label: 'shop'       , uri: '#'            , state: 'shop'  },
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  },
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  },
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  },
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  },
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' }
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ],
            footermenu: [
                {label:'about',     uri: '#/about',         state:'about'},
                {label:'contact',   uri: '#/contact', state:'contact'}
            ]
        };

        // Data
        // ----
        vm.products = {};

        // States
        // ------
        switch ($state.current.name) {
            case 'products':
                vm.products = getProductsAction();
                break;
            default:
                break;
        }

        // Functions
        // =========

        // Products
        // -----

        function getProductsAction() {
            $log.info(getProductsAction.name);

            //vm.$$ui.loader = true;

            return ProductResource
                .query(success, error);

            function error(error) {
                $log.error(getProductsAction.name, 'ERROR', 'error:', error);
                //vm.$$ui.loader = false;

            }

            function success(resource, responseHeader) {
                $log.log(getProductsAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                //vm.$$ui.loader = false;
            }
        }

        function RemoveCartItem(cartitem) {
            _.remove($vm.cartitems, function(c){
                return c.id == cartitem.id;
            });
        }


    }


})();

