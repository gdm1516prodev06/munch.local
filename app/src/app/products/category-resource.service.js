/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.products')
        .factory('CategoryResource', CategoryResource);

    /* @ngInject */
    function CategoryResource(
        // Angular
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('categories/:category_id');
        var paramDefaults = {
            category_id : '@id'
        };

        var actions = {};
        
        return $resource(url, paramDefaults, actions);
    }

})();