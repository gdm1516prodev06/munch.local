/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */

;(function () {
    'use strict';

    angular.module('app.products')
        .controller('DetailController', DetailController);

    /* @ngInject */
    function DetailController(
        // Angular
        $log,
        // Third-party Services
        $state,
        // Custom
        CategoryResource,
        ProductResource
    ) {
        $log.info('DetailController', Date.now());
        $log.log('Current state name:', $state.current.name);

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  },
                { label: 'shop'       , uri: '#'            , state: 'shop'  },
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  },
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  },
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  },
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  },
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' }
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ],
            footermenu: [
                {label:'about',     uri: '#/about',         state:'about'},
                {label:'contact',   uri: '#/contact', state:'contact'}
            ]
        };

        // Data
        // ----
        vm.product = {
        };

        vm.av_rating = [];

        // States
        // ------
        switch ($state.current.name) {
            case 'detail':
                vm.product = getProductAction();
                break;
            default:
                break;
        }

        // Functions
        // =========

        // Products
        // -----

        function getProductAction() {
            $log.log(getProductAction.name);

            var params = {
                product_id: $state.params.product_id
            };

            console.log(params.product_id);
            return ProductResource
                .get(params, success, error);

            function error(error) {
                $log.error(getProductAction.name, 'ERROR', 'error:', error);
            }

            function success(resource, responseHeader) {
                $log.log(getProductAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());

            }

        }

        function getRating(rating){

            console.log(rating);
            var ratingArray = [];
            for (var i = 1; i <= rating; i++) {
                ratingArray.push(i);
            }
            console.log(ratingArray);
            return ratingArray;
        }




    }


})();