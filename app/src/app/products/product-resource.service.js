/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.products')
        .factory('ProductResource', ProductResource);

    /* @ngInject */
    function ProductResource(
        // Angular
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('products/:product_id');

        var paramDefaults = {
            product_id: '@id' // @id is the property, e.g. `product.id` if the resource instance is `product`.
        };

        

        var actions = {
            'update': {
                method: 'PUT'
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();