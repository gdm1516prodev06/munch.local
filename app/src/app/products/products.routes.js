/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.products')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('products', {
                cache: false, // false will reload on every visit.
                controller: 'ProductsController as vm',
                templateUrl: 'html/products/products.view.html',
                url: '/products'
            })
            .state('basket', {
                cache: false, // false will reload on every visit.
                controller: 'ProductsController as vm',
                templateUrl: 'html/products/basket.view.html',
                url: '/products/basket'
            })
            .state('detail', {
                cache: false, // false will reload on every visit.
                controller: 'DetailController as vm',
                templateUrl: 'html/products/detail.view.html',
                url: '/products/{product_id:int}'
            });
    }

})();