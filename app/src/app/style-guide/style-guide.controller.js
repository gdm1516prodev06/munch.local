/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.style-guide')
        .controller('StyleGuideController', StyleGuideController);

    /* @ngInject */
    function StyleGuideController(
        // Angular
        $log,
        $scope,
        // Third-party
        $_,
        $faker
    ) {
        $log.info(StyleGuideController.name, Date.now());

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {
        };

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Style Guide'
        };

        // Data
        // ----
        vm.product = {
            title: null,
            content: null,
            category: null,
            tags: []
        };

        // Functions
        // =========
        
        // Actions
        // -------
        function saveAction() {
            $log.log('saveAction:', vm.product);
        }

        function cancelAction() {
            $log.log('cancelAction');
        }

        // Watchers
        // --------

        // Other
        // -----

    }

})();
