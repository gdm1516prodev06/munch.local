/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */

;(function () {
    'use strict';

    angular.module('app.order')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('information', {
                cache: false, // false will reload on every visit.
                controller: 'OrderController as vm',
                templateUrl: 'html/order/information.view.html',
                url: '/order/information'
            })
            .state('ordercustomer', {
                cache: false, // false will reload on every visit.
                controller: 'OrderController as vm',
                templateUrl: 'html/order/ordercustomer.view.html',
                url: '/order/login_register'
            })
            .state('ordernewcustomer', {
                cache: false, // false will reload on every visit.
                controller: 'OrderController as vm',
                templateUrl: 'html/order/ordernewcustomer.view.html',
                url: '/order/new_customer'
            })
            .state('ordercomplete', {
                cache: false, // false will reload on every visit.
                controller: 'OrderController as vm',
                templateUrl: 'html/order/ordercomplete.view.html',
                url: '/order/ordercomplete'
            });
    }

})();