/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.order')
        .controller('OrderController', OrderController);

    /* @ngInject */
    function OrderController(
        // Angular
        $log
    ) {
        $log.info('OrderController', Date.now());

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  },
                { label: 'shop'       , uri: '#'            , state: 'shop'  },
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  },
                { label: 'groenten'       , uri: '#/products'       , state: 'products'  },
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  },
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  },
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  },
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' }
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ]
        }
    }

})();
