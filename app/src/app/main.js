/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *                                                                         *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Ilona Zancaner 1MMPa
 * @created	   3/05/2016
 * @modified
 * @copyright  Copyright © 2014-2015 Artevelde University College Ghent
 *
 * @function   
 *             
 * 			   
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
 *	Title: Munch App
 *	Modified: 23-03-2016
 *	Version: 1.0.0
 *	Author: Ilona Zancaner
 * 	-----------------------------------------------
 */

(function() {



    // Describe an App object with own functionalities
    var App = {
        init: function() {
            var self = this;

            //this.registerNavigationToggleListeners();// Register All Navigation Toggle Listeners

            //this.registerWindowListeners();// Register All Navigation Toggle Listeners

        },
        registerNavigationToggleListeners: function() {

            var toggles = document.querySelectorAll('.navigation-toggle');
console.log(toggles.length);
            if(toggles != null && toggles.length > 0) {

                var toggle = null;
                alert("klik");
                for(var i = 0; i < toggles.length; i++ ) {
                    toggle = toggles[i];
                    toggle.addEventListener('click', function(ev) {
                        ev.preventDefault();

                        document.querySelector('body').classList.toggle(this.dataset.navtype);
                        document.querySelector('.overlay').classList.toggle('.overlay_show');

                        return false;
                    });
                }
            }
        },
        registerWindowListeners: function() {
            window.addEventListener('resize', function(ev) {
                if(document.querySelector('body').classList.contains('offcanvas-open')) {
                    document.querySelector('body').classList.remove('offcanvas-open');
                }
            });
        }

    };

    App.init();// Intialize the application


})();