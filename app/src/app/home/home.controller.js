/**
 * @author    Ilona Zancaner
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.home')
        .controller('HomeController', HomeController);

    /* @ngInject */
    function HomeController(
        // Angular
        $log
    ) {
        $log.info('HomeController', Date.now());
    
        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};

        // User Interface
        // --------------
        vm.$$ui = {
            mainmenu: [
                { label: 'home'       , uri: '#/'            , state: 'home'  ,class: ''},
                { label: 'shop'       , uri: '#'            , state: 'shop'  ,class: 'listitem-droplist'},
                { label: 'jouw profiel'       , uri: '#/profile'       , state: 'profile'  ,class: ''},
                { label: 'groenten'       , uri: '#/products'       , state: 'products'  ,class: ''},
                { label: 'bestellingen'       , uri: '#/profile/orders'       , state: 'orders'  ,class: ''},
                { label: 'favorieten'       , uri: '#/profile/favorites'       , state: 'favorites'  ,class: ''},
                { label: 'Blog'       , uri: '#/blog'       , state: 'blog.posts'  ,class: ''},
                { label: 'Style Guide', uri: '#/style-guide', state: 'style-guide' ,class: ''}
            ],
            submenu: [
                { label: 'groenten'       , uri: '#/products'            , state: 'vegetables'  ,class: 'listitem-go'},
                { label: 'fruit'       , uri: '#/products'            , state: 'vegetables'  ,class: 'listitem-go'},
                { label: 'vlees'       , uri: '#/products'            , state: 'vegetables'  ,class: 'listitem-droplist'}
            ],
            desktopmenu: [
                { label: 'home'       , uri: '#/'               , state: 'home'  },
                { label: 'shop'       , uri: '#'                , state: 'shop'  }
            ],
            title: 'Home',
            footermenu: [
                {label:'about',     uri: '#/about',         state:'about'},
                {label:'contact',   uri: '#/contact', state:'contact'}
            ],
            homeCategoriesLeft: [
                {label: 'Groenten', source: 'images/groenten.jpg', state:'products.vegetables'},
                {label: 'Vlees', source: 'images/vlees.jpg', state:'products.meat'},
                {label: 'Fruit', source: 'images/fruit.jpeg', state:'products.fruit'},
                {label: 'Zuivel', source: 'images/zuivel.jpg', state:'products.dairy'},
                {label: 'Granen', source: 'images/granen.jpg', state:'products.cereal'}
            ],
            homeCategoriesRight: [

            ]
        }
    }

})();
