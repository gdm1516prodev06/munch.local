#Informatie:
Matthias Seghers & Ilona Zancaner

**Opleidingsonderdeel:** New Media Design & Development II  
**Academiejaar:** 2015-2016  
**Opleiding:** Bachelor in de grafische en digitale media  
**Afstudeerrichting:** Multimediaproductie  
**Keuzeoptie:** proDEV  
**Opleidingsinstelling:** Arteveldehogeschool

#Installatie
```
$ c
$ git clone https://mattsegh1@bitbucket.org/gdm1516prodev06/munch.local.git
$ munch
$ artestead make --type laravel
$ vu
$ vss
vagrant@munch$ composer_update
vagrant@munch$ munch && cd www/
vagrant@munch$ composer create-project
vagrant@munch$ composer update
vagrant@munch$ artisan munch:database:user
vagrant@munch$ artisan munch:database:init --seed
vagrant@munch$ exit
$ _
```